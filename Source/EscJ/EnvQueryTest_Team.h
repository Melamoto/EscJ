// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryTest.h"
#include "EnvQueryTest_Team.generated.h"

/**
 * 
 */
UCLASS()
class ESCJ_API UEnvQueryTest_Team : public UEnvQueryTest
{
    GENERATED_BODY()
public:
    UEnvQueryTest_Team();

    UPROPERTY(EditDefaultsOnly, Category = Team)
    TSubclassOf<UEnvQueryContext> SameTeamAs;

    virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;
	
};
