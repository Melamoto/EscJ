// Fill out your copyright notice in the Description page of Project Settings.

#include "EnvQueryTest_Team.h"
#include "EscJPawn.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_ActorBase.h"
#include "EnvironmentQuery/Contexts/EnvQueryContext_Querier.h"

UEnvQueryTest_Team::UEnvQueryTest_Team() {
    Cost = EEnvTestCost::Low;
    SetWorkOnFloatValues(false);
    ValidItemType = UEnvQueryItemType_ActorBase::StaticClass();
    SameTeamAs = UEnvQueryContext_Querier::StaticClass();
}

void UEnvQueryTest_Team::RunTest(FEnvQueryInstance& QueryInstance) const {
    UObject* QueryOwner = QueryInstance.Owner.Get();
    if (QueryOwner == nullptr)
    {
        return;
    }

    BoolValue.BindData(QueryOwner, QueryInstance.QueryID);
    bool bWantsValid = BoolValue.GetValue();

    TArray<AActor*> ContextActors;
    if (!QueryInstance.PrepareContext(SameTeamAs, ContextActors))
    {
        return;
    }

    for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It) {
        AEscJPawn* Target = Cast<AEscJPawn>(GetItemActor(QueryInstance, It));
        if (Target != nullptr) {
            for (int32 ContextIndex = 0; ContextIndex < ContextActors.Num(); ContextIndex++) {
                AEscJPawn* EscJPawn = Cast<AEscJPawn>(ContextActors[ContextIndex]);
                check(EscJPawn);
                It.SetScore(TestPurpose, FilterType, EscJPawn->Friendlies.Contains(Target), bWantsValid);
            }
        }
        else {
            It.ForceItemState(EEnvItemStatus::Passed);
        }
    }
}
