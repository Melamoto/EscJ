// Fill out your copyright notice in the Description page of Project Settings.

#include "EnvQueryTest_Player.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_ActorBase.h"
#include "EscJPawn.h"

UEnvQueryTest_Player::UEnvQueryTest_Player() {
    Cost = EEnvTestCost::Low;
    SetWorkOnFloatValues(false);
    ValidItemType = UEnvQueryItemType_ActorBase::StaticClass();
}

void UEnvQueryTest_Player::RunTest(FEnvQueryInstance& QueryInstance) const {
    UObject* QueryOwner = QueryInstance.Owner.Get();
    if (QueryOwner == nullptr)
    {
        return;
    }

    BoolValue.BindData(QueryOwner, QueryInstance.QueryID);
    bool bWantsValid = BoolValue.GetValue();

    for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It) {
        AEscJPawn* Target = Cast<AEscJPawn>(GetItemActor(QueryInstance, It));
        if (Target != nullptr) {
            It.SetScore(TestPurpose, FilterType, Target->IsPlayerControlled(), bWantsValid);
        }
        else {
            It.ForceItemState(EEnvItemStatus::Passed);
        }
    }
}
