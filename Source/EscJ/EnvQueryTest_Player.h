// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryTest.h"
#include "EnvQueryTest_Player.generated.h"

/**
 * 
 */
UCLASS()
class ESCJ_API UEnvQueryTest_Player : public UEnvQueryTest
{
	GENERATED_BODY()
public:
    UEnvQueryTest_Player();

    virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;

};
