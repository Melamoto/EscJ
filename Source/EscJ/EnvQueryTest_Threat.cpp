// Fill out your copyright notice in the Description page of Project Settings.

#include "EnvQueryTest_Threat.h"
#include "EscJPawn.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_ActorBase.h"
#include "EnvironmentQuery/Contexts/EnvQueryContext_Querier.h"

UEnvQueryTest_Threat::UEnvQueryTest_Threat() {
    Cost = EEnvTestCost::Low;
    ValidItemType = UEnvQueryItemType_ActorBase::StaticClass();
    ThreatHeldBy = UEnvQueryContext_Querier::StaticClass();
}

void UEnvQueryTest_Threat::RunTest(FEnvQueryInstance& QueryInstance) const {
    UObject* QueryOwner = QueryInstance.Owner.Get();
    if (QueryOwner == nullptr)
    {
        return;
    }

    FloatValueMin.BindData(QueryOwner, QueryInstance.QueryID);
    float MinThresholdValue = FloatValueMin.GetValue();

    FloatValueMax.BindData(QueryOwner, QueryInstance.QueryID);
    float MaxThresholdValue = FloatValueMax.GetValue();

    TArray<AActor*> ContextActors;
    if (!QueryInstance.PrepareContext(ThreatHeldBy, ContextActors))
    {
        return;
    }

    for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It) {
        AEscJPawn* Target = Cast<AEscJPawn>(GetItemActor(QueryInstance, It));
        if (Target != nullptr) {
            for (int32 ContextIndex = 0; ContextIndex < ContextActors.Num(); ContextIndex++) {
                AEscJPawn* EscJPawn = Cast<AEscJPawn>(ContextActors[ContextIndex]);
                check(EscJPawn);
                float* ThreatPtr = EscJPawn->ThreatMap.Find(Target);
                float Threat = ThreatPtr == nullptr ? 0 : *ThreatPtr;
                It.SetScore(TestPurpose, FilterType, Threat, MinThresholdValue, MaxThresholdValue);
            }
        }
        else {
            It.ForceItemState(EEnvItemStatus::Passed);
        }
    }
}
