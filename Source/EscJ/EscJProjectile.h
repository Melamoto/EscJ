// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EscJProjectile.generated.h"

class UProjectileMovementComponent;
class UStaticMeshComponent;

UCLASS(config=Game)
class AEscJProjectile : public AActor
{
	GENERATED_BODY()
public:

	/** Sphere collision component */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Projectile, meta = (AllowPrivateAccess = "true"))
    class USphereComponent* ProjectileCollision;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* ProjectileMovement;

    UPROPERTY(Category = Audio, EditAnywhere, BlueprintReadWrite)
    class USoundBase* ImpactSound;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 InstigatorTeam;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float Damage;

    UFUNCTION(BlueprintImplementableEvent)
    void HitFX(FVector Location, FVector Direction);

public:
	AEscJProjectile();

    void SetFriendlies(TArray<class AEscJPawn*> Friendlies);

	/** Function to handle the projectile hitting something */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns ProjectileMovement subobject **/
	FORCEINLINE UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }
};

