// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EscJPawn.generated.h"

UCLASS(Blueprintable)
class AEscJPawn : public APawn
{
	GENERATED_BODY()

	/* The mesh component */
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* ShipMeshComponent;

	/** The camera */
	UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

    /** Camera boom positioning the camera above the character */
    UPROPERTY(Category = Movement, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
    class UFloatingPawnMovement* FloatingMovement;

public:
    AEscJPawn();

    UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn = true))
    TSubclassOf<class AEscJWeapon> WeaponClass;

    UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
    TSubclassOf<class AEscJProjectile> ProjectileClass;

    UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
    float StartingHealth;

    UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
    float CurrentHealth;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Gameplay)
    TArray<AEscJPawn*> Friendlies;

    class AEscJWeapon* CurrentWeapon;

	/** Offset from the ships location to spawn projectiles */
	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite )
	FVector GunOffset;
	
	/* How fast the weapon will fire */
	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
	float FireRate;

	/* The speed our ship moves around the level */
	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
	float MoveSpeed;

    UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
    float RotationSpeed;

    UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
    int32 TeamId;

	/** Sound to play each time we fire */
	UPROPERTY(Category = Audio, EditAnywhere, BlueprintReadWrite)
	class USoundBase* FireSound;

    UFUNCTION(BlueprintCallable)
    void SetWeapon(TSubclassOf<class AEscJWeapon> WeaponType);

    UFUNCTION(BlueprintImplementableEvent)
    void OnKilled();

    TMap<APawn*, float> ThreatMap;

	// Begin Actor Interface
    virtual void BeginPlay() override;
    virtual void Destroyed() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
    virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	// End Actor Interface

    // Begin Actor Interface
    virtual class UPawnMovementComponent* GetMovementComponent() const override;
    // End Actor Interface

	/* Fire a shot in the specified direction */
    UFUNCTION(BlueprintCallable)
	void FireShot(FVector FireDirection);

	/* Handler for the fire timer expiry */
	void ShotTimerExpired();

    void MoveForward(float AxisValue);
    void MoveRight(float AxisValue);

    void StartFiring();
    void StopFiring();

	// Static names for axis bindings
	static const FName MoveForwardBinding;
	static const FName MoveRightBinding;
	static const FName FireForwardBinding;
	static const FName FireRightBinding;

public:

    uint32 bWantsToFire : 1;

	/* Flag to control firing  */
    UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	uint32 bCanFire : 1;

	/** Handle for efficient management of ShotTimerExpired timer */
	FTimerHandle TimerHandle_ShotTimerExpired;

public:
	/** Returns ShipMeshComponent subobject **/
	FORCEINLINE class UStaticMeshComponent* GetShipMeshComponent() const { return ShipMeshComponent; }
	/** Returns CameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetCameraComponent() const { return CameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
};

