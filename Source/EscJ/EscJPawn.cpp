// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "EscJPawn.h"
#include "EscJProjectile.h"
#include "TimerManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/CollisionProfile.h"
#include "Engine/StaticMesh.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "EscJWeapon.h"
#include "Engine.h"

const FName AEscJPawn::MoveForwardBinding("MoveForward");
const FName AEscJPawn::MoveRightBinding("MoveRight");
const FName AEscJPawn::FireForwardBinding("FireForward");
const FName AEscJPawn::FireRightBinding("FireRight");

AEscJPawn::AEscJPawn()
{	
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShipMesh(TEXT("/Game/TwinStick/Meshes/TwinStickUFO.TwinStickUFO"));
	// Create the mesh component
	ShipMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShipMesh"));
	RootComponent = ShipMeshComponent;
	ShipMeshComponent->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
	ShipMeshComponent->SetStaticMesh(ShipMesh.Object);

    FloatingMovement = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("FloatingMovement"));
    FloatingMovement->MaxSpeed = 1000.f;
    FloatingMovement->Acceleration = 1000.f;
    FloatingMovement->SetPlaneConstraintEnabled(true);
    FloatingMovement->SetPlaneConstraintAxisSetting(EPlaneConstraintAxisSetting::Z);

    WeaponClass = AEscJWeapon::StaticClass();
	
	// Cache our sound effect
	static ConstructorHelpers::FObjectFinder<USoundBase> FireAudio(TEXT("/Game/TwinStick/Audio/TwinStickFire.TwinStickFire"));
	FireSound = FireAudio.Object;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when ship does
	CameraBoom->TargetArmLength = 1200.f;
	CameraBoom->RelativeRotation = FRotator(-80.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false;	// Camera does not rotate relative to arm

    ProjectileClass = AEscJProjectile::StaticClass();

	// Movement
    RotationSpeed = 360.f;
	MoveSpeed = 1000.0f;
	// Weapon
	GunOffset = FVector(90.f, 0.f, 0.f);
	FireRate = 0.1f;
	bCanFire = true;
    bWantsToFire = false;

    StartingHealth = 100.f;

    TeamId = 0;
}

void AEscJPawn::BeginPlay() {
    Super::BeginPlay();
    CurrentHealth = StartingHealth;
    SetWeapon(WeaponClass);
}

void AEscJPawn::Destroyed() {
    Super::Destroyed();
    if (CurrentWeapon != nullptr) {
        CurrentWeapon->Destroy();
    }
}

void AEscJPawn::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);

	// set up gameplay key bindings
    PlayerInputComponent->BindAction(FName("Fire"), EInputEvent::IE_Pressed, this, &AEscJPawn::StartFiring);
    PlayerInputComponent->BindAction(FName("Fire"), EInputEvent::IE_Released, this, &AEscJPawn::StopFiring);
    PlayerInputComponent->BindAxis(MoveForwardBinding, this, &AEscJPawn::MoveForward);
    PlayerInputComponent->BindAxis(MoveRightBinding, this, &AEscJPawn::MoveRight);
	PlayerInputComponent->BindAxis(FireForwardBinding);
	PlayerInputComponent->BindAxis(FireRightBinding);
}

void AEscJPawn::StartFiring() {
    UE_LOG(LogTemp, Warning, TEXT("STARTED FIRING"));
    bWantsToFire = true;
}
void AEscJPawn::StopFiring() {
    UE_LOG(LogTemp, Warning, TEXT("STOPPED FIRING"));
    bWantsToFire = false;
}

float AEscJPawn::TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController* EventInstigator, AActor* DamageCauser) {
    AEscJPawn* PawnDamageCauser = EventInstigator != nullptr ? Cast<AEscJPawn>(EventInstigator->GetPawn()) : nullptr;
    if (PawnDamageCauser != nullptr && PawnDamageCauser->TeamId == TeamId) {
        return 0.f;
    }
    float Damage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
    if (ThreatMap.Contains(PawnDamageCauser)) {
        float* CurrentThreat = ThreatMap.Find(PawnDamageCauser);
        *CurrentThreat += Damage;
    }
    else {
        ThreatMap.Add(PawnDamageCauser, Damage);
    }
    CurrentHealth -= Damage;
    if (CurrentHealth <= 0) {
        OnKilled();
        Destroy();
    }
    return Damage;
}

void AEscJPawn::SetWeapon(TSubclassOf<class AEscJWeapon> WeaponType) {
    if (CurrentWeapon != nullptr) {
        CurrentWeapon->Destroy();
    }
    UWorld* World = GetWorld();
    check(World);
    FActorSpawnParameters ActorSpawnParams;
    ActorSpawnParams.Instigator = this;
    CurrentWeapon = World->SpawnActor<AEscJWeapon>(WeaponType, ActorSpawnParams);
    CurrentWeapon->AttachToActor(this, FAttachmentTransformRules::SnapToTargetIncludingScale);
    CurrentWeapon->Friendlies = Friendlies;
}

void AEscJPawn::Tick(float DeltaSeconds)
{
    Friendlies.RemoveAll([](AEscJPawn* Friendly) {
        return !IsValid(Friendly);
    });
    if (CurrentWeapon != nullptr) {
        CurrentWeapon->Friendlies.RemoveAll([](AEscJPawn* Friendly) {
            return !IsValid(Friendly);
        });
    }
    Friendlies.RemoveAll([](AEscJPawn* Friendly) {
        return !IsValid(Friendly);
    });
    TArray<APawn*> ThreatKeys;
    ThreatMap.GetKeys(ThreatKeys);
    for (auto Threat : ThreatKeys) {
        if (!IsValid(Threat)) {
            ThreatMap.Remove(Threat);
        }
    }

    FVector MoveDirection = GetVelocity();
    if (!MoveDirection.IsNearlyZero()) {
        MoveDirection.Normalize();
        FVector NewFacingDirection = FMath::VInterpNormalRotationTo(GetActorForwardVector(), MoveDirection, DeltaSeconds, RotationSpeed);
        FloatingMovement->MoveUpdatedComponent(FVector::ZeroVector, NewFacingDirection.Rotation(), false);
    }
	
	// Create fire direction vector
	const float FireForwardValue = GetInputAxisValue(FireForwardBinding);
	const float FireRightValue = GetInputAxisValue(FireRightBinding);
	const FVector FireDirection = FVector(FireForwardValue, FireRightValue, 0.f);

	// Try and fire a shot
    if (!FireDirection.IsNearlyZero()) {
        FireShot(FireDirection);
    }
    if (bWantsToFire && bCanFire) {
        APlayerController* OwningController = Cast<APlayerController>(GetController());
        if (OwningController != nullptr) {
            FVector WorldLocation;
            FVector WorldDirection;
            bool Success = OwningController->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);
            if (Success) {
                FVector TargetLocation = FMath::LinePlaneIntersection(WorldLocation, WorldLocation + (WorldDirection * 5000), GetActorLocation(), FVector::UpVector);
                FVector CursorDirection = TargetLocation - GetActorLocation();
                CursorDirection.Normalize();
                FireShot(CursorDirection);
            }
        }
    }
}

UPawnMovementComponent* AEscJPawn::GetMovementComponent() const {
    return FloatingMovement;
}

void AEscJPawn::MoveForward(float AxisValue) {
    GetMovementComponent()->AddInputVector(FVector(AxisValue, 0.f, 0.f));
}
void AEscJPawn::MoveRight(float AxisValue) {
    GetMovementComponent()->AddInputVector(FVector(0.f, AxisValue, 0.f));
}

void AEscJPawn::FireShot(FVector FireDirection)
{
    if (CurrentWeapon != nullptr) {
        CurrentWeapon->FireShot(FireDirection);
    }
}

void AEscJPawn::ShotTimerExpired()
{
	bCanFire = true;
}

