// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Engine.h"
#include "EscJGameMode.generated.h"

USTRUCT(BlueprintType)
struct FEnemyTeamData : public FTableRowBase {
    GENERATED_BODY()

public:
    FEnemyTeamData() : TeamName(""), TeamMaterial(nullptr) {}

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyData)
    FString TeamName;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyData)
    UMaterialInterface* TeamMaterial;
};

UCLASS()
class AEscJGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEscJGameMode();

    int32 TeamCount;

    UFUNCTION(BlueprintCallable)
    void SpawnTeam(TArray<FVector> SpawnPositions, TSubclassOf<AEscJPawn> EnemyType, UMaterialInterface* TeamMaterial);
};



