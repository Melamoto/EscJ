// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EscJWeapon.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent, Blueprintable) )
class ESCJ_API AEscJWeapon : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	AEscJWeapon();

    /** Sound to play each time we fire */
    UPROPERTY(Category = Audio, EditAnywhere, BlueprintReadWrite)
    class USoundBase* FireSound;

    UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
    TSubclassOf<class AEscJProjectile> ProjectileClass;

    /** Offset from the ships location to spawn projectiles */
    UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
    FVector GunOffset;

    /* How fast the weapon will fire */
    UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
    float FireRate;

    TArray<class AEscJPawn*> Friendlies;

    bool bCanFire;

	// Called when the game starts
	virtual void BeginPlay() override;

    virtual void FireShot(FVector FireDirection);

public:	
    void ShotTimerExpired();

private:
    FTimerHandle TimerHandle_ShotTimerExpired;
	
};
