// Fill out your copyright notice in the Description page of Project Settings.

#include "EscJWeapon.h"
#include "EscJProjectile.h"
#include "Engine.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "EscJPawn.h"


// Sets default values for this component's properties
AEscJWeapon::AEscJWeapon()
{
    static ConstructorHelpers::FObjectFinder<USoundBase> FireAudio(TEXT("/Game/TwinStick/Audio/TwinStickFire.TwinStickFire"));
    FireSound = FireAudio.Object;

    ProjectileClass = AEscJProjectile::StaticClass();

    FireRate = 0.1f;
    bCanFire = true;

    GunOffset = FVector(90.f, 0.f, 0.f);
}


// Called when the game starts
void AEscJWeapon::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void AEscJWeapon::FireShot(FVector FireDirection)
{
    // If it's ok to fire again
    if (bCanFire == true)
    {
        // If we are pressing fire stick in a direction
        if (FireDirection.SizeSquared() > 0.0f)
        {
            const FRotator FireRotation = FireDirection.Rotation();
            // Spawn projectile at an offset from this pawn
            const FVector SpawnLocation = GetActorLocation() + FireRotation.RotateVector(GunOffset);

            UWorld* const World = GetWorld();
            check(World)
            // spawn the projectile
            FTransform SpawnTransform(FireRotation, SpawnLocation);
            AEscJProjectile* Projectile = World->SpawnActorDeferred<AEscJProjectile>(ProjectileClass, SpawnTransform, this, Instigator, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
            Projectile->SetFriendlies(Friendlies);
            Projectile->FinishSpawning(SpawnTransform);

            bCanFire = false;
            World->GetTimerManager().SetTimer(TimerHandle_ShotTimerExpired, this, &AEscJWeapon::ShotTimerExpired, FireRate);

            // try and play the sound if specified
            if (FireSound != nullptr)
            {
                UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
            }

            bCanFire = false;
        }
    }
}

void AEscJWeapon::ShotTimerExpired()
{
    bCanFire = true;
}
