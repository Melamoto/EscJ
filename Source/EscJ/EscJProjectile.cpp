// Copyright 1998-2018 Epic Games, Inc. All Rights Reserve

#include "EscJProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "EscJPawn.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SphereComponent.h"
#include "Engine/StaticMesh.h"

AEscJProjectile::AEscJProjectile() 
{
	// Create mesh component for the projectile sphere
    ProjectileCollision = CreateDefaultSubobject<USphereComponent>(TEXT("ProjectileCollision"));
    ProjectileCollision->SetupAttachment(RootComponent);
    ProjectileCollision->BodyInstance.SetCollisionProfileName("Projectile");
    ProjectileCollision->OnComponentHit.AddDynamic(this, &AEscJProjectile::OnHit);		// set up a notification for when this component hits something
	RootComponent = ProjectileCollision;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement0"));
	ProjectileMovement->UpdatedComponent = ProjectileCollision;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->ProjectileGravityScale = 0.f; // No gravity

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

    Damage = 20.f;
}

void AEscJProjectile::SetFriendlies(TArray<class AEscJPawn*> Friendlies) {
    for (auto Friendly : Friendlies) {
        ProjectileCollision->IgnoreActorWhenMoving(Friendly, true);
    }
}

void AEscJProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor != nullptr && OtherActor != this) {
        UGameplayStatics::ApplyPointDamage(OtherActor, Damage, GetVelocity(), Hit, GetInstigatorController(), this, UDamageType::StaticClass());
	}

    FVector Direction = GetVelocity();
    Direction.Normalize();
    HitFX(Hit.ImpactPoint, Direction);
    UGameplayStatics::PlaySoundAtLocation(this, ImpactSound, Hit.ImpactPoint);
	Destroy();
}
