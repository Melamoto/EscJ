// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "EscJGameMode.h"
#include "EscJPawn.h"
#include "Engine.h"

AEscJGameMode::AEscJGameMode()
{
	// set default pawn class to our character class
	DefaultPawnClass = AEscJPawn::StaticClass();
    TeamCount = 2;
}

void AEscJGameMode::SpawnTeam(TArray<FVector> SpawnPositions, TSubclassOf<AEscJPawn> EnemyType, UMaterialInterface* TeamMaterial) {
    TArray<AEscJPawn*> SpawnedEnemies;
    UWorld* World = GetWorld();
    check(World);
    for (auto SpawnPosition : SpawnPositions) {
        AEscJPawn* NewEnemy = World->SpawnActor<AEscJPawn>(EnemyType, SpawnPosition, FRotator::ZeroRotator);
        if (NewEnemy == nullptr) {
            continue;
        }
        NewEnemy->GetShipMeshComponent()->SetMaterial(0, TeamMaterial);
        NewEnemy->TeamId = TeamCount;
        SpawnedEnemies.Add(NewEnemy);
    }
    for (auto Enemy : SpawnedEnemies) {
        Enemy->Friendlies = SpawnedEnemies;
    }
    TeamCount += 1;
}
