// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryTest.h"
#include "EnvQueryTest_Threat.generated.h"

/**
 * 
 */
UCLASS()
class ESCJ_API UEnvQueryTest_Threat : public UEnvQueryTest
{
	GENERATED_BODY()
public:
    UEnvQueryTest_Threat();

    UPROPERTY(EditDefaultsOnly, Category = Team)
    TSubclassOf<UEnvQueryContext> ThreatHeldBy;

    virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;
};
